package com.example.self

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import android.R.attr.path




class MainActivity : AppCompatActivity() {
    var CameraRequestCode = 0
    lateinit var Name: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var NameEdit = findViewById<EditText>(R.id.Edit_name)
        var Btn = findViewById<Button>(R.id.Send_btn)
        Btn.setOnClickListener(View.OnClickListener {
            if(NameEdit.text.toString()!="") {
            Name = NameEdit.text.toString()
            var Camera = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                if (Camera.resolveActivity(packageManager) != null) {
                startActivityForResult(Camera, CameraRequestCode)
                }
            }
            else{
                Toast.makeText(this,getString(R.string.not_found_name),Toast.LENGTH_SHORT).show()
            }

        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode){
            CameraRequestCode->{
                if(resultCode==Activity.RESULT_OK&&data!=null){
                  var  photo = data.extras.get("data") as Bitmap
                    var intent = Intent(applicationContext, PhotoActivity::class.java)
                    intent.putExtra(Intent.EXTRA_TEXT, Name)
                    intent.putExtra("image", photo )
                    startActivity(intent)
                }
            }
            else ->{
                Toast.makeText(this,getString(R.string.not_found_photo), Toast.LENGTH_SHORT).show()
            }
        }

    }
}
