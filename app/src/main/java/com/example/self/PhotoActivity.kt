package com.example.self

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView



class PhotoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_photo)
       var Name =findViewById<TextView>(R.id.TakeName)
       var Photo = findViewById<ImageView>(R.id.TakePhoto)
        Name.text = intent.getStringExtra(Intent.EXTRA_TEXT)
        Photo.setImageBitmap(intent.getParcelableExtra("image"))
    }
}
